## Pré-requis et dépendance

- [node js](https://nodejs.org/fr/download/)

## Installer les dépendances

Aller dans le répertoire du projet puis executez la commande suivante pour charger le dépendance : 

```
npm install
```

## Démarrer react js 

Aller dans le répertoire du projet puis executez la commande suivante dans votre `Terminal` ou `Shell`:

```
npm start
```

## Ract JS démarre généralement sur le port 

```
http://localhost:3000
```
