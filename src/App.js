import logo from './logo.svg';
import './App.css';
import { Component } from 'react';

class App extends Component {

  state = {
  posts: []
}

componentDidMount(){
  fetch('http://localhost:5500/cars')
  .then((response) => response.json())
  .then(cars => this.setState({posts: cars}));
}

render(){
  return (
    <div className="App">
      <h1>Listes des véhicules : </h1>
       <ul>
      { this.state.posts.map(post => (<li key={post._id}> <img src={post.image} width="200" /> <h2>{post.marque} - {post.modele}</h2> <button>Voir détail</button> </li>)) }
       </ul>
    </div>
  )
}

}

export default App;
